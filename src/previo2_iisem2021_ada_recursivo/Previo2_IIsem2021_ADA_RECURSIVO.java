/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo2_iisem2021_ada_recursivo;

/**
 *
 * @author COLOQUE ACÁ SU NOMBRES COMPLETOS
 */
public class Previo2_IIsem2021_ADA_RECURSIVO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Vector de ejemplo:
        int v[]={3,4,5,6};
        System.out.println(imprimir(v, v.length-1,"[", -1, v.length-2));
        
        //LLamado de su método:
    }
    
    public static String imprimir(int [] v, int i, String msg, int j, int k){
        if(i>0){
            msg+=v[i]+",";
            return imprimir(v, --i, msg, j, k);
        }
        if(i==0){
            msg+=v[i]+"]";
            msg+="\n[";
            return imprimir(v, -1, msg, k, --k);
        }
        if(j>0){
            msg+=v[j]+",";
            return imprimir(v, i, msg, --j, k);
        }
        if(j==0){
            msg+=v[j]+"]";
            if(k>=0){
                msg+="\n[";
                return imprimir(v, i, msg, k, --k);
            } else {
                return msg;
            }
            
        }
        
            
            
        return "";
    }
    
}
